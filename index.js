// Enter to staging BD (command arg:: robomongo &):
// Acceder a server
// Acceder a DB
// Coinsenda client|user server
// sudo ssh -i /home/coinsenda/.ssh/ohio-phi.pem -L 27017:localhost:27017 ubuntu@18.189.141.230
// Protocolo para re activar servicios

// 1. ------------ verificar servicios en marcha ------------
// sudo docker ps

// 1.1 ------------si hay servicios, tumbarlos ------------
// sudo docker-compose -f coinsenda-development-compose.yml down

// 2. ------------ Borrar logs ------------
// sudo rm -rf /var/lib/docker /etc/docker

// 3. ------------ Reiniciar server ------------
// sudo reboot now

// 4. ------------ Iniciar dockers ------------
// sudo docker-compose -f coinsenda-development-compose.yml up -d  || "dentro del directorio que contiene la referencia .yml"

// Si no funciona a la primera, repetir desde el punto 2


// ROBO MONGO ROBO3T
// cd /usr/local/bin...       //SEARCH INTO PATH THE NEXT APP
// ./robo3t
// db.getCollection('authClient').find({"name": "backoffice"})


// AUTH
// Client Id's:
// senda: 60654a646cf0d700d17f5cb1
// admin: 60654a646cf0d700d17f5cb5
// Host: http://auth.bitsenda.com/signin?clientId=
// Senda: http://auth.bitsenda.com/signin?clientId=6067f5a9bdd72d00d1076365
// mail:construyacol+p0001@gmail.com
// userId:6067f778bdd72d00d1076371
// BackOffice: http://auth.bitsenda.com/signin?clientId=6067f5a9bdd72d00d1076366

// search in bd db.getCollection('profile').find({"userId" : ObjectId("6067f778bdd72d00d1076371")})

// try enter with 2fa


// DOCKER
//AUTH:: Into "auth server" folder init in mongo/rabbit
// with run BD
// yarn start:dev
// http://127.0.0.1:3000/signin?clientId=607a719db8f19c27e44687f3

// development: yarn wp:dev
  // http://localhost:8080/signin?clientId=607a719db8f19c27e44687f3

// deploy CURL
curl -X POST 'https://supervisor.theeye.io/indophi/webhook/5d4cc52aeed38b000f2d7874/trigger/secret/5ae27a2a04bcca6821707803d2368dd4de642b555ca2af149ee4ab551e47f024'
// consola docker auth-service
docker exec -it auth-service bash

// into 'auth server/' initialize mongo && rabbit with:
// sudo docker-compose up -d
// sudo docker ps
// sudo docker-compose down


// Antes de realizar cualquier cambio en las ramas remotas se deben de tumbar los dockers
1. Bajar los docker del auth
// sudo docker-compose -f only-auth-local-compose.yml down
2. Si hay cambios, actualizar rama remota y hacer pull desde quay, si se va a hacer referencia a otra rama cambiar
en coinsenda-deploy/only-auth-local-compose.yml el nombre de la rama
// docker pull quay.io/indophi/auth:[branch_name]
// docker pull quay.io/indophi/auth:moving_to_loopback

3. Levantar dockers
// sudo docker-compose -f only-auth-local-compose.yml up -d
4. Ejecutar Logs
// docker logs --follow auth-service




































// Add AWS invalidations
// El invalidation se agrega después de hacer cada deploy

https://indophi.signin.aws.amazon.com/console
andres-staging
.Andres123

// 1.Entrar a consola de AWS
// 2.Buscar y entrar en el servicio de cloudfront
// 3.Clickear en el ID del dominio en el que desea borrar el caché
// 4.ir a invalidations
// 5.create invalidation: "/*"




ADMIN

// solo bastaría tener level_3 en el profile del tx y el rol admin en el user del auth service para que ande todo bien en admin no?






















// CORDOVA

// Android studio
// cd /opt/
// cd android-studio/
// cd bin/
// ./studio.sh

// Ejecutar script .sh ===========================================================
// crear archivo .sh "myScript.sh" con los comandos a ejecutar
// otorgar permisos al .sh con el siguiente comando: "chmod +x myScript.sh"
// ===============================================================================


// configurar atom como editor preestablecido
// git config --global core.editor "'C:\Users\User\AppData\Local\atom\bin\atom.cmd' --wait"

// INSERTANDO EN RAMA develop

// $ ls = $ dir

// $ clear = cls

// $ rm nombre_del_archivo.extensión = del /Q nombre_del_archivo.extensión

// $ rm -rf nombre_de_la_carpeta = rd /S /Q nombre_de_la_carpeta

// $ touch archivo.extencion = fsutil file createnew archivo.extencion 0
// El cero es el tamaño, se podría cambiar para crear un archivo con otro tamaño

// Para renombrar archivos es $ ren nombredelarchivo.extensión nombredelarchivo2.extensión, en linux es:
// $ mv nombredelarchivo.extensión nombredelarchivo2.extensión

// Para renombrar carpetas es $ move /Y nombredelacarpeta nombredelacarpeta2, en linux es:
// $ mv nombredelacarpeta nombredelacarpeta2

// crear archivo
// COPY CON archivo1.txt
// finalizar: CTRL + z
// ver contenido archivo: TYPE archivo1.txt
// modificar archivo: notepad archivo1.txt










// GIT COMMANDS------------------------------------------------------------------------------------------------------------------------------------------

// eliminar todos los archivos del staggin: git reset -q.




// -------------- GIT TAG


// git tag: nos permite agregar etiquetas a nuestros cambios.
// -a para la anotación
// -m para el mensaje
// En este momento quizás ya quieras taggear o etiquetar los cambios. Para eso existe git tag, este simplemente solo va a agregar una etiqueta.
// Una cosa a destacar es que son ligeras o anotadas, las ligeras se crean con el nombre de la etiqueta, las anotadas son las que llevan además un mensaje usando -m podemos dejar un mensaje.
// Si queremos borrar una etiqueta debemos usar -d, si las queremos listar podemos usar -l y si las queremos renombrar usamos -f.
// Utilizando el Sha-1 podemos crear etiquetas anotadas haciendo referencia a commits viejos.

// -l nos muestra la lista de etiquetas
// -f para renombrar
// -d para borrar
// Vale la pena resaltar que no se pueden renombrar los tags lightweight. Al utilizar git tag -f v0.3 [commit_ID] lo que hará es crear un tag nuevo.
// Para estos casos se debe borrar el tag con git tag -d [anotacion] y crearlo nuevamente con git tag [anotacion].

// ej: git tag -a [anotación sha]






// -------------- GIT LOG
// git log --oneline: Coloca los commits de manera resumida y en una sola línea.
// –graph: Nos mostraria los diferentes commits en las ramas o bifurcaciones con un asterisco.
// -[numero]: Nos permite ver los últimos commits
// git log archivo[extension] => muestra el historial de cambios de un archivo
// Documentacion de git log: https://www.git-scm.com/docs/git-log
// git log --stat: permite ver los archivos que se cambiaron en los commits
// git reflog te muestra absolutamente toda la historia del proyecto incluso aunque hayas borrados ramas etc...



// -------------- GIT DIFF
// Otra herramienta que podemos usar con git es la revisión de los cambios. Para esto vamos a usar el comando git diff.
// Si queremos saber cuáles son los cambios que hemos tenido entre un commit y otro, solo debemos hacer una comparación entre un commit y otro usando Sha-1.

// git diff [SHA1]: Nos muestra las cambios de ese commit.
// rojo: fueron cambios que se quitaron
// verde: se agregaron cosas
// git diff [XXX-1] [XXX-2] : Muestra las diferencias del commit [XXX-1] contra el commit [XXX-2]







// -------------- git reset -- soft
// Alguna vez tuviste un error y quisiste borrar el commit que habías enviado. Para esto podemos hacer a través de git reset. Si quieres usar este comando debes tener mucho cuidado, porque puedes borrar muchos commits.
// El comando reset --soft nos permite identificar el commit que queremos resetear.
// Hay que tener en cuenta que si usas git reset --soft
// Tienes 10 commits y borras el numero 7, los commits 8, 9 y 10 también se borran, y tendras un nuevo commit que tomara el numero 8.

// git reset --soft [SHA-1]

// Cuando aplicas reset --soft al commit numero 7, los commit 8, 9 y 10 se vuelven commits de una rama(branch) alterna a la MASTER, son borrados de la rama principal, pero puedes acceder a ellos como como una rama alterna.
// Para visualizar lo descrito anteriormente usar git superlog, ya que usando git log, solo puedes acceder a la rama MASTER o a la rama donde has hecho checkout

// git reset --soft [SHA 1]: elimina los cambios hasta el staging area
// git reset --mixed [SHA 1]: elimina los cambios hasta el working area
// git reset --hard [SHA 1]: regresa hasta el commit del [SHA 1]

// git reset HEAD: nos ayuda a sacar archivos del estado Staged (git add .) para devolverlos a su estado anterior. Si los archivos venían de Unstaged, vuelven allí. Y lo mismo se venían de Untracked.
 // Este es el comando para sacar archivos del área de Staging. No para borrarlos ni nada de eso, solo para que los últimos cambios de estos archivos no se envíen al último commit, a menos que cambiemos de opinión y los incluyamos de nuevo en staging con git add, por supuesto.

// El comando git checkout + ID del commit nos permite viajar en el tiempo. Podemos volver a cualquier versión anterior de un archivo específico o incluso del proyecto entero. Esta también es la forma de crear ramas y movernos entre ellas.
// También hay una forma de hacerlo un poco más “ruda”: usando el comando git reset. En este caso, no solo “volvemos en el tiempo”, sino que borramos los cambios que hicimos después de este commit.
// Hay dos formas de usar git reset: con el argumento --hard, borrando toda la información que tengamos en el área de staging (y perdiendo todo para siempre). O, un poco más seguro, con el argumento --soft, que mantiene allí los archivos del área de staging para que podamos aplicar nuestros últimos cambios pero desde un commit anterior.






// -------------- git branch
// Las ramas son muy importantes si quieres trabajar con un equipo y no quieres tocar la rama master para no crear conflictos,

// git branch [nombre] se crea una nueva rama
// -l: listamos las ramas
// -d/-D [nombre]: borramos rama
// -m [nombre] [nombre_nuevo]: para renombrar ramas
    // d -> delete
    // D -> delete forzado
    // m -> move/rename
    // M -> move/rename forzado
    // l -> listar
    // f -> forzar

    // Eliminar una rama en el repositorio remoto ----------------------------------------- -------------------------------------------

    // git push origin :[nombre-rama]
    // **git branch -v **
    // para listar las ramas y mostrar sus commits








    // -------------- git checkout-----------------------------------------------------------------------------------------------------

    // para crear una rama apartir de lo que tenga otra:
    // git checkout -b nuevaRama master
    // git checkout -b nuevaRama => de esta forma hace un fork de la rama actual y crea la nueva rama especificada
    // git checkout -- file.extension => descartar o resetear los cambios del archivo
    // git branch (sin flags) … es equivalente a git branch -l, es decir, lista las ramas existentes.

// https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
// Les comparto un enlace donde se explica muy bien un modelo que podemos utilizar en nuestros proyectos para facilitar la integración de los cambios mediante el uso de ramas.
// http://nvie.com/posts/a-successful-git-branching-model/


  // El comando git checkout [commit_id] [archivo(opcional)] nos permite viajar en el tiempo. Podemos volver a cualquier versión anterior de un archivo específico o incluso del proyecto entero. Esta también es la forma de crear ramas y movernos entre ellas.
  // git checkout [master] [archivo(opcional)] Nos permite volver a la versión master más reciente del repositorio





// -------------- git merge --abort-----------------------------------------------------------------------------------------------------


   // git merge --abort
   // solo se puede ejecutar después de que la fusión haya generado conflictos. git merge --abort abortará el proceso de fusión e intentará reconstruir el estado previo a la fusión.












// para pisar a partir de x coomit en el repositorio remoto, esto es peligroso ya que borra a partir del commit actual en adelante

// git push —force origin master




// --------------git stash (Guardando cambios temporalmente)----------------------------------------------------------------------------------------------------

// ¿Qué tal si aún no estás listo para confirmar ningún cambio? Stash es un estado que tenemos como intermedio. Para esto debemos ir a alguna de nuestras ramas y usando
// el comando git stash que nos permite hacer cambios, pero no confirmarlos.



// git stash: es otro de los limbos, como el staging area. Para agregar los cambios estos deben estar en el staging area.
// git stash list: nos muestra la lista de stash que tengamos.
// git stash drop stash@{numero}: nos permite borrar un stash.
// git stash apply: aplicamos el último cambio


// --------------git cherry-pick----------------------------------------------------------------------------------------------------
// Si estás trabajando en una rama, pero de repente notas que hiciste un cambio en la rama que no debías, para esto podemos usar cherry pick.
// Este comando nos puede salvar la vida, ya que nos permite sacar cambios específicos de una rama y mezclarlos en otra.
// Si por error se genera una solucion (commit) en una rama que aun no deseamos fusionar pero es de urgencia procesar dicho cambio (unicamente ese commit)
 // se puede hacer uso de cherry pick para copiar el commit a una nueva rama y fusionarla sin los commits adicionales a la rama master
// Para usar este comando, debemos estar ubicados en la rama destino.


// git cherry-pick [SHA1] nos permite cambiar un commit a otra rama para salvarnos la vida.




// markDown estilos .MD
// https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf
// Template merge request
// https://docs.gitlab.com/ee/user/project/description_templates.html


    //Nueo cambio para nuevo merge request



// --------------git remote ----------------------------------------------------------------------------------------------------


// Tenemos que crear una conexión entre tu repositorio local y tu repositorio remoto para que puedas compartirlo con la comunidad.
// Para esto vamos a usar un nuevo comando que en este caso es git remote. Por convención lo nombramos “origin”. Para borrarlo podemos usar
// git remote remove y el nombre del repositorio.

// Como buenas prácticas, se recomienda lo siguiente:
// El nombre del repositorio creado en Github (u otra plataforma) debe ser el mismo que el nombre de la carpeta del proyecto.
// Primero crear el repositorio remoto en Github, luego clonarlo al repositorio local en tu máquina y de ahí empezar a trabajar.



// git remote add [origin] [SSH/HTTPS] Conecta un repositorio con nuestro equipo local.
// git remote -v Lista las conexiones existentes.
// git remote remove [origin] Elimina una conexión con algún repositorio.



// A veces hacemos un commit, pero resulta que no queríamos mandarlo porque faltaba algo más. Utilizamos git commit --amend, amend en inglés es remendar y lo que hará es que los cambios que hicimos nos lo agregará al commit anterior.
// git commit --amend
// recuerda que debes hacer el git add . antes de hacer el commit siempre


// Buscar palabras contenidas en archivos, útil para saber donde se está llamando un metodo que debe ser retirado del proyecto
// 1. Nos posicionamos dentro del directorio que deseamos buscar. Ej: /proyecto/src/
// git grep [Palabra a buscar]









// eslint-disable-next-line react-hooks/exhaustive-deps



















//
